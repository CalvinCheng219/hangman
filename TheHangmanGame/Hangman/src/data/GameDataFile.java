package data;

import com.fasterxml.jackson.databind.util.JSONPObject;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.*;
import java.nio.file.Path;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ui.AppMessageDialogSingleton;


/**
 * @author Ritwik Banerjee, Calvin cheng
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) {
        JSONObject obj = new JSONObject();
        GameData gd = ((GameData) data);
      //  System.out.println(gd.get)
        obj.put("targetWord",gd.getTargetWord());
        obj.put("remainingGuesses", new Integer(gd.getRemainingGuesses()));

        JSONArray goodGuess = new JSONArray();
        Iterator<Character> goodIt = gd.getGoodGuesses().iterator();
        while(goodIt.hasNext()){
            goodGuess.add("" + goodIt.next());
        }

        JSONArray badGuess = new JSONArray();
        Iterator<Character> badIt = gd.getBadGuesses().iterator();
        while(badIt.hasNext()){
            badGuess.add("" + badIt.next());
        }

        obj.put("goodGuesses",goodGuess);
        obj.put("badGuesses",badGuess);
        obj.put("hintable",gd.getHintable());

        try{
            FileWriter file = new FileWriter(to.toFile());
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        }
        catch (IOException e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Saving Error", "There was a problem with the file");
        }




    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        JSONParser parser = new JSONParser();
        try {

            Object obj = parser.parse(new FileReader(from.toFile()));
            JSONObject jsonObject = (JSONObject) obj;
            //GameData gd = (GameData)data;
            ((GameData)data).setTargetWord((String)jsonObject.get("targetWord"));
            long guessDummy = (long) jsonObject.get("remainingGuesses");
            ((GameData)data).setRemainingGuesses(((int)guessDummy));
           // gd.setRemainingGuesses((int)jsonObject.get("remainingGuesses"));
            ((GameData)data).setHintable(((boolean)jsonObject.get("hintable")));
            JSONArray goodGuesses = (JSONArray) jsonObject.get("goodGuesses");
            Iterator<Character> iterator = goodGuesses.iterator();
            ((GameData)data).getGoodGuesses().clear();
            while (iterator.hasNext()) {
                ((GameData)data).getGoodGuesses().add(iterator.next());
            }

            JSONArray badGuesses = (JSONArray) jsonObject.get("badGuesses");
            Iterator<Character> badIterator = badGuesses.iterator();
            ((GameData)data).getBadGuesses().clear();
            while (badIterator.hasNext()) {
                ((GameData)data).getBadGuesses().add(badIterator.next());
            }



 //           System.out.println("all done loading");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
