package gui;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;


/**
 * @author Calvin cheng
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label      guiHeadingLabel;   // workspace (GUI) heading label
    HBox       headPane;          // conatainer to display the heading
    GridPane bodyPane;          // container for the main game displays
    ToolBar    footToolbar;       // toolbar for game buttons
    BorderPane figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox       gameTextsPane;     // container to display the text-related parts of the game
    HBox       guessedLetters;    // text area displaying all the letters guessed so far
    GridPane       goodGuessBox;
    HBox        badGuessBox;
    Canvas hangmanBody;
    HBox       remainingGuessBox; // container to display the number of remaining guesses
 //   HBox       messageBox;        // container for all in game messages
    Button     startGame;         // the button to start playing a game of Hangman
    Button hintButton;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        initApp.setWorkspaceComponent(this);
        gui = app.getGUI();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }
    public GridPane getGoodGuessBox(){
        return goodGuessBox;
    }
    public HBox getBadGuessBox(){
        return badGuessBox;
    }
    public Canvas getCanvas(){
        return hangmanBody;
    }
    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
     //   Group root = new Group();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        hangmanBody = new Canvas(300,300);

        figurePane.getChildren().add(hangmanBody);
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
   //     messageBox = new HBox();
        gameTextsPane = new VBox();
        goodGuessBox = new GridPane();
        goodGuessBox.setStyle("-fx-background-color: transparent;");
       // badGuessBox = new HBox();
      //  badGuessBox.setStyle("-fx-background-color: transparent;");
        hintButton = new Button ("Hint");
        hintButton.setVisible(false);
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,hintButton);

        bodyPane = new GridPane();
       // bodyPane.setPadding(new Insets(10,500,300,10));

        bodyPane.add(gameTextsPane,0,0);
        bodyPane.add(figurePane,1,0);
        bodyPane.add(goodGuessBox,2,0);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

      //  Scene workspaceScene = new Scene(root, 800,500);
    //    workspaceScene.
        workspace = new VBox(20);
      //  VBox.set
        workspace.getChildren().addAll(headPane,bodyPane, footToolbar);
    }

    private void setupHandlers() {
        //HangmanController controller = new HangmanController(app, startGame);
        HangmanController controller = (HangmanController)gui.getFileController();
        controller.setProperties(startGame, hintButton);
        startGame.setOnMouseClicked(e -> controller.start());
        hintButton.setOnMouseClicked(e -> controller.hint());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {

    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

  //  public HBox getMessageBox() {return messageBox; }

    public Button getStartGame() {
        return startGame;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
   //     messageBox = new HBox();

        goodGuessBox = new GridPane();
        goodGuessBox.setStyle("-fx-background-color: transparent;");
      //  badGuessBox = new HBox();
    //    badGuessBox.setStyle("-fx-background-color: transparent;");
        hintButton.setVisible(false);

        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, hintButton);
        bodyPane.getChildren().clear();
        bodyPane.add(gameTextsPane,0,0);
        bodyPane.add(figurePane,1,0);
        bodyPane.add(goodGuessBox,2,0);
        bodyPane.getColumnConstraints().clear();
        bodyPane.getColumnConstraints().add(new ColumnConstraints(500));
        bodyPane.getColumnConstraints().add(new ColumnConstraints(300));
        bodyPane.getColumnConstraints().add(new ColumnConstraints(200));
        bodyPane.getRowConstraints().clear();
        bodyPane.getRowConstraints().add(new RowConstraints(300));
        /*
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col2 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col3 = new ColumnConstraints();
        col1.setPercentWidth(33);
        bodyPane.getColumnConstraints().addAll(col1,col2,col3);
        */
        GraphicsContext gc = hangmanBody.getGraphicsContext2D();
        gc.clearRect(0,0,205,205);
    }
}
