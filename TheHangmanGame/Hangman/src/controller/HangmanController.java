
/**
 * @author Ritwik Banerjee, Calvin cheng
 */
package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.geometry.Insets;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.TextDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.CharacterCodingException;
import java.nio.file.Path;
import java.io.File;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private Text[]      progress;    // reference to the text area for the word
    private Text        goodGuessText;
    private Text        badGuessText;
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hintButton;
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
//    private Label       message;     // message label pertaining to game information
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private boolean loadable;
    private Path        workFile;
    private String      savedFile;
    private HBox[] alphabet;
    private boolean hintable;

    public File                  currentWorkFile; // the file on which currently work is being done
    public boolean saved;

    public HangmanController(AppTemplate appTemplate, Button gameButton, Button hintButton) {
        this(appTemplate);
        this.gameButton = gameButton;
        this.hintButton = hintButton;
        gameover = true;
        saved = false;
        currentWorkFile = null;  //do i even need to do this?!?!
    }

    public int getDistinctLeft(){
        int counter = 0;
        Set<Character> distinct = new HashSet<>();
        for (int i = 0; i < progress.length; i++){
            if (!distinct.contains(progress[i].getText().toString().charAt(0))&& !progress[i].isVisible()){
                distinct.add(progress[i].getText().toString().charAt(0));
            }
        }
        return distinct.size();
    }

    public void hint(){
        boolean Guessed = true;
        int index = 0;
        while (Guessed){
            index = new Random().nextInt(progress.length);
            if (!progress[index].isVisible()){
                Guessed = false;
            }
        }
        char hintChar = progress[index].getText().toString().charAt(0);
        for(int i = 0; i < progress.length; i++){

            if ((progress[i].getText().toString().charAt(0)+"").compareToIgnoreCase(""+hintChar)==0){
           // if (progress[i].getText().toString().charAt(0) == hintChar){
                progress[i].setVisible(true);
                discovered++;
            }
        }
        gamedata.addGoodGuess(hintChar);
        gamedata.subRemainingGuesses();
        updateGuessBox();
        updateHangman();
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        success = (discovered == progress.length);
        hintable = false;
        gamedata.setHintable(hintable);
        updateHintButton(hintable);
        savable = true;
        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);
    }
    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }
    public void setProperties( Button gameButton, Button hintButton) {
        this.gameButton = gameButton;
        this.hintButton = hintButton;
        gameover = true;
        saved = false;
        loadable = false;
        hintable = true;
        currentWorkFile = null;  //do i even need to do this?!?!

        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }
    public void updateHintButton(boolean hintable){
        hintButton.setDisable(!hintable);
    }
    public void updateHangman(){
        GameData gd = (GameData)appTemplate.getDataComponent();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        GraphicsContext gc = gameWorkspace.getCanvas().getGraphicsContext2D();
        //
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(2);
        gc.clearRect(0,0,205,205);
        switch(gd.getRemainingGuesses()){
            case 0: gc.strokeLine(70, 150, 100, 130);
            case 1: gc.strokeLine(130, 150, 100, 130);
            case 2:gc.strokeLine(100, 110, 130, 125);
            case 3:gc.strokeLine(100, 110, 70, 125);
            case 4:gc.strokeLine(100, 100, 100, 130);
            case 5:gc.strokeOval(80, 60, 40, 40);
            case 6: gc.strokeLine(100, 40, 100, 60);
            case 7:gc.strokeLine(50, 40, 100, 40);
            case 8: gc.strokeLine(50, 180, 50, 40);
            case 9:gc.strokeLine(20, 180, 180, 180);
            case 10: gc.strokeRoundRect(0,  0, 200, 200, 10, 10);
        }
    }
    public void start() {
        gamedata = new GameData(appTemplate);
        gameover = false;
        success = false;
        savable = true;
        hintable = true;
        gamedata.setHintable(hintable);
        updateHintButton(hintable);
        discovered = 0;
        hintable = true;
        hintButton.setVisible(true);
        loadable = true;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
     //   HBox messageBox = gameWorkspace.getMessageBox();

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
     //   messageBox.getChildren().addAll(message);
        initWordGraphics(guessedLetters);
        gameButton.setDisable(true);
        if(getDistinctLeft() >7 ){
            hintButton.setVisible(true);
        }
        else {
            hintButton.setVisible(false);
        }

            play();

    }

    private void end() {
     //  AppMessageDialogSingleton messageBox = AppMessageDialogSingleton.getSingleton();
        hintable = false;
        updateHintButton(hintable);
        if (success){
            remains.setText(remains.getText() + "\nCongragulations, YOU WIN!!!");
       }
        else {
       //     messageBox.show("Defeat", "Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\".");
            remains.setText(remains.getText() + "\nAh, close but not quite there");
            for(int i = 0; i < progress.length; i++){
                if (!progress[i].isVisible()){
                    progress[i].setVisible(true);
                    progress[i].setFill(Color.RED);
                    progress[i].setFont(Font.font("Verdana", FontWeight.BOLD, 12));
                }
            }
        }
//        System.out.println(success ? "You win!" : "Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\".");
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is alrelady over
        loadable = false;
        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);

    }

    private void initWordGraphics(HBox guessedLetters) {
        alphabet = new HBox[26];
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        for(int i = 0; i < progress.length; i++){
            HBox dummy = new HBox();
            dummy.getChildren().add(progress[i]);
            dummy.setPadding(new Insets(5, 10, 5, 10));
            dummy.setSpacing(10);
            dummy.setStyle("-fx-background-color: #e1d027;");
            guessedLetters.getChildren().add(dummy);
            HBox spaceholder = new HBox();
            spaceholder.getChildren().add(new Text(" "));
            guessedLetters.getChildren().add(spaceholder);
        }
    //    goodGuessText = new Text("Available Letters: ");
  //      badGuessText = new Text("Bad Guesses: ");
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
    //    HBox goodBox = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
     //   goodBox.getChildren().add(goodGuessText);
   //     gameWorkspace.getGoodGuessBox().getChildren().add(goodGuessText);


 //       gameWorkspace.getBadGuessBox().getChildren().add(badGuessText);

        gameWorkspace.getGoodGuessBox().setVgap(10.0);
        gameWorkspace.getGoodGuessBox().setHgap(10.0);
        int counter = 0;
        for (int i = 0; i < 5; i++){
            gameWorkspace.getGoodGuessBox().getColumnConstraints().add(new ColumnConstraints(20));
            gameWorkspace.getGoodGuessBox().getRowConstraints().add(new RowConstraints(20));
            for (int j = 0; j < 5; j++){
                alphabet[counter] = new HBox();
                alphabet[counter].getChildren().add(new Text("" + (char)(counter+97)));
                alphabet[counter].setStyle("-fx-background-color: #5BFF33;");
                alphabet[counter].setPadding(new Insets(5, 5, 5, 5));
                gameWorkspace.getGoodGuessBox().add(alphabet[counter],j,i);
                counter++;
            }
        }
        alphabet[25] = new HBox();
        alphabet[25].getChildren().add(new Text("" + (char)(counter+97)));
        alphabet[25].setStyle("-fx-background-color: #5BFF33;");
        alphabet[counter].setPadding(new Insets(5, 5, 5, 5));
        gameWorkspace.getGoodGuessBox().add(alphabet[25],0,5);
      //  HBox badBox = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(3);
      //  goodBox.getChildren().add(badGuessText);
      //  guessedLetters.getChildren().addAll(progress);
    }
    public void updateGuessBox(){
        GameData gd = (GameData)appTemplate.getDataComponent();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        Object[] goodChar = gamedata.getGoodGuesses().toArray();
        Object[] badChar = gamedata.getBadGuesses().toArray();
        char dummy;
        for (int i = 0; i<goodChar.length; i++){
            dummy = goodChar[i].toString().charAt(0);
            alphabet[(int)dummy - 97].setStyle("-fx-background-color: #000000;");
        }
        for (int i = 0; i<badChar.length; i++){
            dummy = badChar[i].toString().charAt(0);
            alphabet[(int)dummy - 97].setStyle("-fx-background-color: #000000;");
        }
        /*
        if(gd.getGoodGuesses().toString().length()>2){
            goodGuessText.setText("Good Guesses:\t\t" +gd.getGoodGuesses().toString().substring(gd.getGoodGuesses().toString().indexOf("[")+1,gd.getGoodGuesses().toString().indexOf("]")));
         }
        if(gd.getBadGuesses().toString().length()>2){
            badGuessText.setText("Bad Guesses:\t\t" +gd.getBadGuesses().toString().substring(gd.getBadGuesses().toString().indexOf("[")+1,gd.getBadGuesses().toString().indexOf("]")));
        }
        */


    }
    public void play() {

        updateHangman();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if (!alreadyGuessed(guess) && validGuess(guess)) {
                        boolean goodguess = false;
                        if ( guess >= 65 && guess <= 90) {
                            guess = (char)(guess + 32);
                        }
                        for (int i = 0; i < progress.length; i++) {
                            if ((""+gamedata.getTargetWord().charAt(i)).compareToIgnoreCase(""+guess)==0){
                                progress[i].setVisible(true);

                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                                if(getDistinctLeft() ==1 ){
                                    hintable = false;
                                    updateHintButton(hintable);
                                }
                            }
                        }
                        if (!goodguess)
                            gamedata.addBadGuess(guess);

                        savable = true;
                        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);
                        updateGuessBox();
                        updateHangman();

                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));

                        //message.setText(" ");
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        if ((int)c <= 90 && (int)c>=65){
  //          System.out.print( c + " is detected, turned into " + (char)((int)c+32));
            c = (char)((int)c+32);
        }
        String goodGuessString = gamedata.getGoodGuesses().toString();
        String fixedGoodString = new String("");
        for (int i = 0; i < goodGuessString.length(); i++){
            if (((int)goodGuessString.charAt(i) >= 97 && (int)goodGuessString.charAt(i) <= 122) ||
                    ((int)goodGuessString.charAt(i) >= 65 && (int)goodGuessString.charAt(i) <= 90)){
                fixedGoodString = fixedGoodString + goodGuessString.charAt(i);
            }
        }
    //    System.out.println(fixedGoodString);
        char goodDummy;
        boolean goodHas = false;
        for (int i = 0; i < fixedGoodString.length(); i++){
            goodDummy = fixedGoodString.charAt(i);
            if(goodDummy == c){
                goodHas = true;
                break;
            }
        }

        String badGuessString = gamedata.getBadGuesses().toString();
        String fixedBadString = new String("");
        for (int i = 0; i < badGuessString.length(); i++){
            if (((int)badGuessString.charAt(i) >= 97 && (int)badGuessString.charAt(i) <= 122) ||
                    ((int)badGuessString.charAt(i) >= 65 && (int)badGuessString.charAt(i) <= 90)){
                fixedBadString = fixedBadString + badGuessString.charAt(i);
            }
        }
        char badDummy;
        boolean badHas = false;
        for (int i = 0; i < fixedBadString.length(); i++){
            badDummy = fixedBadString.charAt(i);
            if(badDummy == c){
                badHas = true;
                break;
            }
        }

    //    System.out.println (" already guessed " + (goodHas || badHas));
        return goodHas || badHas;
    }

    private boolean validGuess (char c){
        if (((int)c >= 97 && (c <= 122) ||
                c >= 65 && c <= 90)){
            return true;
        }
        return false;
    }
    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (savable)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            currentWorkFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
            savable = false;
            loadable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable,loadable);
        }

        if (gameover) {
            savable = false;
            loadable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable,loadable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }

    private void saveWork(File selectedFile) throws IOException {
        appTemplate.getFileComponent()
                .saveData(appTemplate.getDataComponent(), Paths.get(selectedFile.getAbsolutePath()));

        currentWorkFile = selectedFile;
        saved = true;

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }
    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            if (currentWorkFile!= null)
                saveWork(currentWorkFile);
            else {
                FileChooser fileChooser = new FileChooser();


            //    boolean test = new File(AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter()).mkdir();
               // System.out.println(AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter()));
                URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

                File initialDir = new File(workDirURL.getFile());

               // File initialDir = new File("C:\\Users\\Andy\\Documents\\TheHangmanGame\\TheHangmanGame\\Hangman\\saved");
                fileChooser.setInitialDirectory(initialDir);
                fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
                        propertyManager.getPropertyValue(WORK_FILE_EXT)));
                File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    String ext = "";
                   // System.out.println("this one " + selectedFile.toString());
                    int i = selectedFile.toString().lastIndexOf('.');
                    if (i > 0) {
                        ext = selectedFile.toString().substring(i+1);
                    }
                    if(ext.equalsIgnoreCase("json")){
                        saveWork(selectedFile);
                        savable = false;
                        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);

                    }
                    else{
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Save Error","Invalid file format");
                    }

                }
            }
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
         //   ioe.printStackTrace();
        }
    }

    @Override
    public void handleLoadRequest()throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        try {

            FileChooser fileChooser = new FileChooser();

            URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");

            File initialDir = new File(workDirURL.getFile());

            // File initialDir = new File("C:\\Users\\Andy\\Documents\\TheHangmanGame\\TheHangmanGame\\Hangman\\saved");
            fileChooser.setInitialDirectory(initialDir);
            fileChooser.setTitle("Which Resource File");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
                    propertyManager.getPropertyValue(WORK_FILE_EXT)));
            File selectedFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null) {
                try {
                    String ext = "";
                    // System.out.println("this one " + selectedFile.toString());
                    int i = selectedFile.toString().lastIndexOf('.');
                    if (i > 0) {
                        ext = selectedFile.toString().substring(i+1);
                    }
                    if(ext.equalsIgnoreCase("json")){
                        appTemplate.getFileComponent()
                                .loadData(appTemplate.getDataComponent(), Paths.get(selectedFile.getAbsolutePath()));

                        handleLoadWorkspace();
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Load", "Sucessfully loaded");
                    }
                    else{
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Save Error","Invalid file format");
                    }
                } catch (IOException ioe) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Load Error", "There was something wrong with the file");
                }
            }
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Load Error", "Please select a file");
        }



    }

    public void handleLoadWorkspace(){
        appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace

        gamedata = (GameData)appTemplate.getDataComponent();
        hintable = gamedata.getHintable();
        updateHintButton(hintable);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        guessedLetters.getChildren().clear();
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        if(getDistinctLeft() >=7 ){
            hintButton.setVisible(true);
        }
        else {
            hintButton.setVisible(false);
        }
        char[] targetDummy = gamedata.getTargetWord().toCharArray();
        for (int i = 0; i < progress.length; i++) {
            progress[i].setVisible(false);
        }
        for(int i = 0; i < progress.length; i++){
            HBox dummy = new HBox();
            dummy.getChildren().add(progress[i]);
            dummy.setPadding(new Insets(5, 10, 5, 10));
            dummy.setSpacing(10);
            dummy.setStyle("-fx-background-color: #e1d027;");
            guessedLetters.getChildren().add(dummy);
            HBox spaceholder = new HBox();
            spaceholder.getChildren().add(new Text(" "));
            guessedLetters.getChildren().add(spaceholder);
        }



        String goodGuessString = gamedata.getGoodGuesses().toString();
        String fixedGoodString = new String("");
        for (int i = 0; i < goodGuessString.length(); i++){
            if (((int)goodGuessString.charAt(i) >= 97 && (int)goodGuessString.charAt(i) <= 122) ||
                    ((int)goodGuessString.charAt(i) >= 65 && (int)goodGuessString.charAt(i) <= 90)){
                fixedGoodString = fixedGoodString + goodGuessString.charAt(i);
            }
        }
      //  System.out.println(fixedGoodString);
       // System.out.println(gamedata.getTargetWord());
        char goodDummy;
        for (int i = 0; i < fixedGoodString.length(); i++){
            goodDummy = fixedGoodString.charAt(i);
            for (int j = 0; j < gamedata.getTargetWord().length(); j++){
                if ((""+gamedata.getTargetWord().charAt(j)).compareToIgnoreCase(""+goodDummy)==0){
     //           if (gamedata.getTargetWord().charAt(j) == goodDummy){
         //           System.out.println(gamedata.getTargetWord().charAt(j) + "=" + goodDummy);
                    progress[j].setVisible(true);
                    discovered++;
                }
            }
        }
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
      //  guessedLetters.getChildren().addAll(progress);
        if(getDistinctLeft() ==1 ){
            hintable = false;
            updateHintButton(hintable);
        }

        for (int i = 0; i<26; i++){
            alphabet[i].setStyle("-fx-background-color: #5BFF33;");
        }


        updateGuessBox();
        updateHangman();




//        guessedLetters.getChildren().addAll(progress);
  //      play();
   //     System.out.print("buffer");
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable)
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    public void handleInfoRequest() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show("Information", "Hangman\nBy Calvin Cheng");
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }
    
    private boolean promptToSave() throws IOException {
  //      System.out.print("i am being prompted!!");
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            if (currentWorkFile != null)
                saveWork(currentWorkFile);
            else {
                FileChooser filechooser = new FileChooser();

                URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

               File initialDir = new File(workDirURL.getFile());
     //           File initialDir = new File("C:\\Users\\Andy\\Documents\\TheHangmanGame\\TheHangmanGame\\Hangman\\saved");

                filechooser.setInitialDirectory(initialDir);
                filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));

                String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
                String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(String.format("%s (*.%s)", description, extension),
                        String.format("*.%s", extension));
                filechooser.getExtensionFilters().add(extFilter);
                File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    String ext = "";
                    // System.out.println("this one " + selectedFile.toString());
                    int i = selectedFile.toString().lastIndexOf('.');
                    if (i > 0) {
                        ext = selectedFile.toString().substring(i+1);
                    }
                    if(ext.equalsIgnoreCase("json")){
                        saveWork(selectedFile);
                    }
                    else{
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Save Error","Invalid file format");
                    }

                }
            }
        }

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {

    }
}
